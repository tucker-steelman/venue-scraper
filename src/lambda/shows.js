require( 'dotenv' ).config()

const MongoClient = require( 'mongodb' ).MongoClient

exports.handler = function( event, context, callback ) {
    const client = new MongoClient( process.env.MONGO_URI, { useNewUrlParser: true } )

    client.connect( () => {
        const db = client.db( 'venue-scraper' )
        const collection = db.collection( 'shows' )

        collection.find( {} ).toArray( ( err, docs ) => {
            let shows = []

            docs.forEach( show => {
                shows.push( {
                    date: show.date,
                    title: show.title,
                    link: show.link,
                    venue: show.venue
                } )
            } )

            callback( null, {
                statusCode: 200,
                body: JSON.stringify( shows )
            } )

            client.close()
        } )
    } )
}