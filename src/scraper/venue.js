const request = require( 'sync-request' )
const cheerio = require( 'cheerio' )

class Venue {
    constructor( name, url, scraper ) {
        this.name = name
        this.url = url
        this.scraper = scraper
    }

    get body() {
        return request( 'GET', this.url ).getBody().toString()
    }

    scrape() {
        let data = []
        let body = this.body

        try {
            return this.scraper( data, cheerio.load( body ) )
        } catch( e ) {
            if ( e instanceof TypeError ) {
                console.error( `scraper for ${ this.name } is not a function` )
            }

            return data
        }
    }
}

module.exports = Venue