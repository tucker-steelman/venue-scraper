module.exports = [
    {
        url: 'https://bellyup.com/calendar',
        scraper( data, $ ) {
            $( '.frontgateEventItem' ).each( ( i, el ) => {
                if ( $( el ).find( 'h2[itemprop="name"] > a' ).text() ) {
                    data.push( {
                        date: $( el ).find( '[itemprop="startDate"]' ).attr( 'content' ),
                        title: $( el ).find( 'h2[itemprop="name"] > a' ).text(),
                        link: `https://bellyup.com${ $( el ).find( 'h2[itemprop="name"] > a' ).attr( 'href' ) }`,
                        venue: 'Belly Up'
                    } )
                }
            } )

            return data
        }
    },
    {
        url: 'https://musicboxsd.com/calendar',
        scraper( data, $ ) {
            $( '.frontgateEventItem' ).each( ( i, el ) => {
                if ( $( el ).find( 'h2[itemprop="name"] > a' ).text() ) {
                    data.push( {
                        date: $( el ).find( '[itemprop="startDate"]' ).attr( 'content' ),
                        title: $( el ).find( 'h2[itemprop="name"] > a' ).text(),
                        link: `https://musicboxsd.com${ $( el ).find( 'h2[itemprop="name"] > a' ).attr( 'href' ) }`,
                        venue: 'Music Box'
                    } )
                }
            } )

            return data
        }
    }
]