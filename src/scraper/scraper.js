require( 'dotenv' ).config()

const fs = require( 'fs' )
const MongoClient = require( 'mongodb' ).MongoClient

const Venue = require( './venue' )
const sites = require( './config' )

const client = new MongoClient( process.env.MONGO_URI, { useNewUrlParser: true } )

client.connect( () => {
    const db = client.db( 'venue-scraper' )
    const shows = db.collection( 'shows' )
    const data = scrape( sites )

    shows.insertMany( data, {}, err => {
        if ( err ) {
            console.log( err )
        }
    } )

    client.close();
} )

function scrape( sites, destination ) {
    let data = []

    for ( let site of sites ) {
        const venue = new Venue( site.venue, site.url, site.scraper )
        data = data.concat( venue.scrape() )
    }

    return data
}