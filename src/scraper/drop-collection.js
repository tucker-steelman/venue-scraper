require( 'dotenv' ).config()

const MongoClient = require( 'mongodb' ).MongoClient

const client = new MongoClient( process.env.MONGO_URI, { useNewUrlParser: true } )

client.connect( () => {
    const db = client.db( 'venue-scraper' )
    
    db.dropCollection( 'shows', {}, err => {
        if ( err ) {
            console.log( err )
        }

        client.close()
    } )
} )